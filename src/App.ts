import * as express from 'express';
export class App {
    public express;

    constructor() {
        this.express = express();
        this.mountRoutes();
    }

    mountRoutes() {
        const router = express.Router();
        router.get('/', (req, res) => {
            res.json({
                message: 'Api works'
            });
        })
        this.express.use('/', router);
    }
}