import { MoviesController } from '../controllers/movies.controller';
import { App } from '../../app';
export class Routes {
    constructor(moviesController: MoviesController, app: App) {
        app.express
            .get('/movies', (req, res) => moviesController.getAllMovies(req, res))
            .get('/movies/random', (req, res) => moviesController.getRandomMovie(req, res))
            .post('/movies', (req, res) => moviesController.postMovie(req, res))

    }
}