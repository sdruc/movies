import { Movie } from "../models/movie.model";
import { IMovieService } from "../services/imovie.service";

export class MoviesController {
    constructor(private movieService: IMovieService) {
        this.getAllMovies.bind(this);
        this.postMovie.bind(this);
    }

    getAllMovies(req, res) {
        res.json(this.movieService.getAllMovies());
    }

    postMovie(req, res) {
        this.movieService.addMovie(req.body)
            .then(_ => {
                res.status(201).end()
            })
            .catch(err => {
                res.status(400).send({ "error": err });
            });

    }


    getRandomMovie(req: any, res: any) {
        let movies: Movie[] = [];
        let durationTime: number;
        if (req.body.durationTime) {
            durationTime = parseInt(req.body.durationTime);
            if (isNaN(durationTime)) {
                res.status(404).send({"error": "Duration Time must have type number"});
                return;
            }
        }
        if (req.body.genres) {
            const combinedGenres = this.movieService.getSortedPowersetGenres(req.body.genres);
            combinedGenres.forEach(genres => {
                movies.push(...this.movieService.getRandomMovie(durationTime, genres));
            });
        } else {
            movies = this.movieService.getRandomMovie(durationTime);
        }
        let moviesSet = new Set(movies);
        res.status(200).send({ movies: [...moviesSet] });
    }
}