import { Movie } from "../models/movie.model";

export interface IMovieService {
    getAllMovies(): Movie[]
    addMovie(movie: Movie): Promise<any>
    getRandomMovie(durationTime?: number, genres?: string[]): Movie[];
    getSortedPowersetGenres(movieGenres: string[])
}