import { MovieService } from "./movie.service";
import { Movie } from "../models/movie.model";
const mockDatabase = {
    genres: ["Comedy", "Drama"],
    movies: [
        {
            "id": 1,
            "title": "Black Swan",
            "year": "2010",
            "runtime": "108",
            "genres": [
                "Drama",
                "Thriller"
            ],
            "director": "Darren Aronofsky",
            "actors": "Natalie Portman, Mila Kunis, Vincent Cassel, Barbara Hershey",
            "plot": "A committed dancer wins the lead role in a production of Tchaikovsky's \"Swan Lake\" only to find herself struggling to maintain her sanity.",
            "posterUrl": "https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg"
        },
        {
            "id": 2,
            "title": "Black Swan",
            "year": "2010",
            "runtime": "119",
            "genres": [
                "Drama"
            ],
            "director": "Darren Aronofsky",
            "actors": "Natalie Portman, Mila Kunis, Vincent Cassel, Barbara Hershey",
            "plot": "A committed dancer wins the lead role in a production of Tchaikovsky's \"Swan Lake\" only to find herself struggling to maintain her sanity.",
            "posterUrl": "https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg"
        },
        {
            "id": 3,
            "title": "Black Swan",
            "year": "2010",
            "runtime": "111",
            "genres": [
                "Thriller"
            ],
            "director": "Darren Aronofsky",
            "actors": "Natalie Portman, Mila Kunis, Vincent Cassel, Barbara Hershey",
            "plot": "A committed dancer wins the lead role in a production of Tchaikovsky's \"Swan Lake\" only to find herself struggling to maintain her sanity.",
            "posterUrl": "https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg"
        },
        {
            "id": 4,
            "title": "Black Swan",
            "year": "2010",
            "runtime": "50",
            "genres": [
                "Comedy"
            ],
            "director": "Darren Aronofsky",
            "actors": "Natalie Portman, Mila Kunis, Vincent Cassel, Barbara Hershey",
            "plot": "A committed dancer wins the lead role in a production of Tchaikovsky's \"Swan Lake\" only to find herself struggling to maintain her sanity.",
            "posterUrl": "https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg"
        },
    ]
}
const movieService = new MovieService(mockDatabase);

test('getRandomMovie should return randomMovie', () => {
    var movies = movieService.getRandomMovie();
    expect(Array.isArray(movies)).toBe(true);
    expect(movies.length).toBe(1);
});
test('getRandomMovie should return movies with runtime > duration-10 && runtime < duration+10', () => {
    const durationTime = 110;
    var movies = movieService.getRandomMovie(durationTime);
    expect(Array.isArray(movies)).toBe(true);
    expect(movies.length).toBe(3);
    movies.map(movie => {
        expect(parseInt(movie.runtime)).toBeGreaterThanOrEqual(durationTime - 10);
        expect(parseInt(movie.runtime)).toBeLessThanOrEqual(durationTime + 10);
    });
});

test('getRandomMovie should return movies with runtime > duration-10 && runtime < duration+10 and with specified genres', () => {
    const durationTime = 110;
    const genres = ["Thriller", "Drama"];
    var movies = movieService.getRandomMovie(durationTime, genres);
    expect(Array.isArray(movies)).toBe(true);
    expect(movies.length).toBe(1);
    movies.map(movie => {
        expect(parseInt(movie.runtime)).toBeGreaterThanOrEqual(durationTime - 10);
        expect(parseInt(movie.runtime)).toBeLessThanOrEqual(durationTime + 10);
    });
});
test('getRandomMovie should return movies with specified genres', () => {
    const durationTime = 110;
    const genres = ["Thriller", "Drama"];
    var movies = movieService.getRandomMovie(null, genres);
    expect(Array.isArray(movies)).toBe(true);
    expect(movies.length).toBe(1);
});