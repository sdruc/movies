import { Movie } from '../models/movie.model';
import * as fs from 'fs';
import e = require('express');
import { IMovieService } from './imovie.service';
export class MovieService implements IMovieService {

    constructor(private database) {
    }

    getAllMovies(): Movie[] {
        return this.database.movies;
    }

    addMovie(movie: Movie): Promise<any> {
        try {
            this.validateModel(movie);
        } catch (err) {
            return Promise.reject(err.message);
        }
        movie.id = this.database.movies.length + 1;
        this.database.movies.push(movie);
        fs.writeFile("src/data/db.json", JSON.stringify(this.database), 'utf-8', (err) => {
            if (err !== null || err !== undefined) {
                return Promise.resolve();
            }
        }
        );
        return Promise.resolve();
    }
    private validateModel(movie: Movie) {
        const modelProperties = [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'title',
                type: 'string',
                required: true,
                maxLength: 255
            },
            {
                name: 'year',
                type: 'number',
                required: true
            },
            {
                name: 'runtime',
                type: 'number',
                required: true
            },
            {
                name: 'director',
                type: 'string',
                required: true,
                maxLength: 255
            },
            {
                name: 'actors',
                type: 'string'
            },
            {
                name: 'genres',
                type: 'string'
            },
            {
                name: 'actors',
                type: 'string'
            },
            {
                name: 'plot',
                type: 'string'
            },
            {
                name: 'posterUrl',
                type: 'string'
            }
        ];
        const keys = Object.keys(movie);
        const requiredProperties = modelProperties.filter(prop => prop.required);
        requiredProperties.map(prop => {
            if (!keys.includes(prop.name)) {
                throw new Error(`${prop.name} is required`);
            }
        });
        movie.genres.map(genre => {
            if (!this.database.genres.includes(genre)) {
                throw new Error(`Genre ${genre} is not found in database`);
            }
        });
        keys.forEach(element => {
            const property = modelProperties.find(prop => prop.name == element);
            if (movie[element] === undefined || movie[element] === null || movie[element] === '') {
                throw new Error(`Property ${element} is required`);
            }
            if (!modelProperties.map(properties => properties.name).includes(element)) {
                throw new Error(`Property ${element} is not defined in Movie model`);
            }
            if (movie[element].length > property.maxLength) {
                throw new Error(`Property ${property.name} max length is: ${property.maxLength}`);
            }
            if (property.name === 'genres') {
                if (!Array.isArray(movie[element]))
                    throw new Error(`Property ${property.name} must be Array`);
                movie[element].map(genre => {
                    if (typeof (genre) !== property.type) {
                        throw new Error(`Genre array item must be strings`);
                    }
                });
            }
            else if (typeof (movie[element]) !== property.type) {
                throw new Error(`Property ${property.name} must be ${property.type}`)
            }
        });
    }

    getRandomMovie(durationTime?: number, genres?: string[]): Movie[] {
        if (!genres && !durationTime) {
            let movieId = Math.floor(Math.random() * this.database.movies.length);
            return [this.database.movies[movieId]];
        } else {
            return this.database.movies.filter(movie => this.movieHasSameGenres(movie.genres, genres) && this.inclueDurationTime(movie, durationTime));
        }
    }
    private inclueDurationTime(movie: Movie, durationTime: number): boolean {
        if (!durationTime) {
            return true;
        } else {
            let runtime = parseInt(movie.runtime);
            return runtime >= durationTime - 10 && runtime <= durationTime + 10;
        }
    }

    private movieHasSameGenres(movieGenres: string[], genres: string[]): boolean {
        if (!genres) {
            return true;
        } else {
            for (let i = 0; i < movieGenres.length; i++) {
                const item = movieGenres[i];
                if (!genres.includes(item) || genres.length !== movieGenres.length) {
                    return false;
                }
            }
            return true;
        }
    }

    getSortedPowersetGenres(movieGenres: string[]) {
        let powerSetGenres = this.powerSet(movieGenres);
        powerSetGenres.sort((a: any[], b: any[]) => {
            if (a.length < b.length) {
                return 1;
            } else {
                return -1;
            }
        });
        return powerSetGenres;
    }
    private powerSet(genres: string[]) {
        let combinedGenres = [[]];
        for (let i = 0; i < genres.length; i++) {
            let len = combinedGenres.length;
            for (let x = 0; x < len; x++) {
                combinedGenres.push(combinedGenres[x].concat(genres[i]))
            }
        }
        return combinedGenres;
    }


}