import { Routes } from './api/routes/routes';
import { MoviesController } from './api/controllers/movies.controller';
import { App } from './app';
import * as database from './data/db.json';
import * as bodyParser from 'body-parser';
import { MovieService } from './api/services/movie.service';

const port = 3000;
var app = new App();
app.express.use(bodyParser.urlencoded({
    extended: true
}));
app.express.use(bodyParser.json());
app.express.listen(port, (err) => {
    if (err) {
        return console.log(err);
    }
    return console.log(`server is listening on port ${port}`);
})
var routes = new Routes(new MoviesController(new MovieService(database)), app)