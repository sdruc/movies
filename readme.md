# Before start:
- `npm install`

# To run:
- `npm start:watch(nodemon)` or `npm:start`

# Docs:
GET /movies - returns all movies from db  
GET /movies/random - durationTime - optional number, genres - optional string[] - returns movies with specified duration && specified genres or return single random movie.  
POST /movies - returns 201 while movie is added to db, 400 if body is invalid.


